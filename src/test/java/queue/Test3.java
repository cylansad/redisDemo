package queue;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import util.ConvertUtil;
import util.ShardedJedisUtil;

public class Test3 {

	public static void main(String[] args) throws InterruptedException {
		ShardedJedisUtil.set("ticketCount", String.valueOf(5)); // 设置票数（set ticketCount 5）

		while (true) {
			List<String> contents = ShardedJedisUtil.brpop(0, "queue");
			if (CollectionUtils.isNotEmpty(contents)) {
				String key = contents.get(0);
				if (StringUtils.equals(key, "queue")) {
					String msg = contents.get(1);

					int ticketCount = ConvertUtil.intOf(ShardedJedisUtil.get("ticketCount"));
					if (ticketCount > 0) {
						System.out.println(msg + "已获取到ticket,当前余票数量：" + (ticketCount - 1));
						ShardedJedisUtil.set("ticketCount", String.valueOf(ticketCount - 1));
					} else {
						System.out.println(msg + "未获取获取到ticket");
					}
				}
			}
		}
	}
}
