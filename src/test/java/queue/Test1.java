package queue;

import util.ShardedJedisUtil;

public class Test1 {

	public static void main(String[] args) {
		for (int i = 0; i < 2000; i++) {
			new MyThread("序号" + i).start();
		}
	}

	static class MyThread extends Thread {
		private String name;

		public MyThread(String name) {
			this.name = name;
		};

		public void run() {
			ShardedJedisUtil.lpush("queue", name);
		}
	}

}
