package delayQueue;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import util.ShardedJedisUtil;

public class Test1 {

	/**
	 * <pre>
	 * 		ZREVRANGEBYSCORE：
	 * 			返回有序集中指定分数区间内的所有的成员。有序集成员按分数值递减(从大到小)的次序排列。
	 * 			命令基本语法：ZREVRANGEBYSCORE key max min [WITHSCORES] [LIMIT offset count]
	 * 				ZREVRANGEBYSCORE	(必须)	
	 * 				key					(必须)	有序集合键名称
	 * 				max					(必须)	最大分数值
	 * 				min					(必须)	最小分数值
	 * 				WITHSCORES			(非必须)	将成员分数一并返回
	 * 				LIMIT				(非必须)	返回结果是否分页,指令中包含LIMIT后offset、count必须输入
	 * 				offset				(非必须)	返回结果起始位置
	 * 				count				(非必须)	返回结果数量
	 * 
	 * 		ZREM：
	 * 			用于移除有序集中的一个或多个成员，不存在的成员将被忽略。
	 * 			命令基本语法：ZREM key member [member ...]
	 * 
	 * 		TODO
	 * 			1. 如果doHandleMsg在处理个别任务时出现异常，会导致循环退出
	 * 			2. 如果多个线程都在监听队列，可能会被重复消费
	 * </pre>
	 */
	public static void main(String[] args) {
		while (true) {
			Set<String> contentList = ShardedJedisUtil.zrevrangeByScore("delay_queue", System.currentTimeMillis(), 0);
			if (CollectionUtils.isNotEmpty(contentList)) {
				for (String content : contentList) {
					doHandleMsg(content);
				}
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 处理消息
	 */
	private static void doHandleMsg(String content) {
		ShardedJedisUtil.zrem("delay_queue", content);

		System.out.println(content);
	}
}
