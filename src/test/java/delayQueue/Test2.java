package delayQueue;

import util.ShardedJedisUtil;

public class Test2 {

	/**
	 * 
	 */
	public static void main(String[] args) {
		ShardedJedisUtil.zadd("delay_queue", System.currentTimeMillis() + (10 * 1000), "hello, 10s"); // 延迟10秒
		ShardedJedisUtil.zadd("delay_queue", System.currentTimeMillis() + (5 * 1000), "hi，5s"); // 延迟5秒
		ShardedJedisUtil.zadd("delay_queue", System.currentTimeMillis() + (8 * 1000), "ok，8s"); // 延迟8秒
	}
}
