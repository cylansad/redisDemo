import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {

	public static int x = 0;

	public static void main(String[] args) throws InterruptedException {
		ExecutorService exe = Executors.newFixedThreadPool(100);

		for (int i = 0; i < 10; i++) {
			exe.execute(new MyThread());
		}
		exe.shutdown();

		while (true) {
			if (exe.isTerminated()) {
				System.out.println(x);
				break;
			}
			Thread.sleep(200);
		}
	}

	static class MyThread extends Thread {
		public void run() {
			for (int i = 0; i < 100; i++) {
				x++;
			}
		}
	}
}
