package pubsub;

import redis.clients.jedis.JedisPubSub;

public class RedisMsgSubListener extends JedisPubSub {

	private String name;

	public RedisMsgSubListener(String name) {
		this.name = name;
	}

	public void onMessage(String channel, String message) {
		System.out.println(this.name + " -> " + message);
	}
}
