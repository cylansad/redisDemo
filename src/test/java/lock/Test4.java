package lock;

import util.JdbcUtil;

/**
 * 20个人同时去自助机购票（数据库锁）
 * 
 * @author sad
 *
 */
public class Test4 {

	public static void main(String[] args) {
		// String sql1 = "create table t_ticket(ticket_count number)";
		// JdbcUtil.executeUpdate(sql1);
		//
		// String sql2 = "insert into t_ticket(ticket_count) values(5)";
		// JdbcUtil.executeUpdate(sql2);

		// String sql3 = "drop table t_ticket";
		// JdbcUtil.executeUpdate(sql3);

		for (int i = 0; i < 20; i++) {
			new Thread4().start();
		}
	}
}

class Thread4 extends Thread {
	public void run() {
		String sql = "update t_ticket set ticket_count=ticket_count-1 where ticket_count>0";
		int r = JdbcUtil.executeUpdate(sql);
		if (r > 0) {
			System.out.println(Thread.currentThread().getId() + " --已购买到票");
		} else {
			System.out.println(Thread.currentThread().getId() + " --余票不足没有买到票");
		}
	}
}