package lock;

import util.ConvertUtil;
import util.ShardedJedisUtil;

/**
 * 5个人排队购票（单线程）
 * 
 * @author sad
 *
 */
public class Test1 {

	public static void main(String[] args) {
		ShardedJedisUtil.set("ticketCount", String.valueOf(2)); // 设置票数（set ticketCount 2）

		// 5个人排队购票（单线程）
		for (int i = 0; i < 5; i++) {
			int ticketCount = ConvertUtil.intOf(ShardedJedisUtil.get("ticketCount"));
			if (ticketCount > 0) {
				ShardedJedisUtil.set("ticketCount", String.valueOf(ticketCount - 1));
				System.out.println("序号" + i + "获取到ticket");
			} else {
				System.out.println("序号" + i + "没有获取到ticket");
			}
		}
	}
}
