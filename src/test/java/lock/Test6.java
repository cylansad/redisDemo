package lock;

import util.ShardedJedisUtil;

public class Test6 {

	public static void main(String[] args) {
		ShardedJedisUtil.lock("lock");

		for (int i = 0; i < 60; i++) {
			new Thread6().start();
		}
	}
}

class Thread6 extends Thread {
	public void run() {
		// setnx，在指定的 key 不存在时，为 key 设置指定的值。设置成功返回1，设置失败返回0。
		boolean getLock = ShardedJedisUtil.lock("lock");
		if (!getLock) {
			return;
		}
		System.out.println("do something");
		ShardedJedisUtil.unlock("lock");
	}
}